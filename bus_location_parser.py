import csv
import requests

Marsh = '37'
GaragNumbers = sorted(
    ['1106', '1108', '1125', '1103', '1151', '1101', '1150', '1115', '1116', '1119', '1152', '1109', '1102',
     '1153', '1104', '1123', '1154', '1120', '1124'])

# url = 'http://data.kzn.ru:8082/api/v0/dynamic_datasets/bus.json'
# r = requests.get(url)
# print(r.json()[0]['data']) #
# print([i['data']['GaragNumb'] for i in r.json() if i['data']['Marsh'] == Marsh])

for GaragNumb in GaragNumbers:
    url = f'http://data.kzn.ru:8082/api/v0/dynamic_datasets/bus/{GaragNumb}.json'
    json = requests.get(url).json()
    data = json['data']
    if data['Marsh'] == Marsh:
        with open(f'/home/ubuntu/data-{GaragNumb}.csv', 'a') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',')
            spamwriter.writerow([data['TimeNav'], data['Latitude'], data['Longitude']])
            break


# * * * * *  /usr/bin/python3 ~/bus_location_parser.py >> ~/cron.log 2>$
